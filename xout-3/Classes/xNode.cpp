//
//  xNode.cpp
//  xout-3
//
//  Created by Lewis Booker on 5/14/14.
//
//

#include "xNode.h"

const float DIFFICULTY = 0.7;

xNode* xNode::createOther(const string& filename, float timeout, Color3B color)
{
	auto node = (xNode*)Sprite::create(filename);
	node->setColor(GREEN);
	node->initX(timeout);
	return node;
}

xNode* xNode::createX(const string& filename, float timeout, int maxTaps)
{
	auto node = createX(filename, timeout);
	node->maxTaps = maxTaps;
	if (maxTaps == 2)
	{
		node->setColor(GREEN);
	}
	if (maxTaps == 3)
	{
		node->setColor(YELLOW);
	}
	node->tapCount = 0;
	return node;
}

xNode* xNode::createX(const std::string& filename, float timeout)
{
    auto node = (xNode*)Sprite::create(filename);
    node->initX(timeout);
	node->retain();
    return node;
}

void xNode::initX(float timeout)
{
    this->scheduleOnce(schedule_selector(xNode::timeExpired), timeout);
    this->scheduleOnce(schedule_selector(xNode::warning), timeout * 0.8);
    this->isHit = false;
    this->tapCount = 0;
	this->maxTaps = 0;
	this->_timeExpired = false;
}

bool xNode::getTimeExpired()
{
    return _timeExpired;
}

void xNode::warning(float dx)
{
    this->setColor(Color3B(200, 100, 50));
}

void xNode::timeExpired(float dt)
{
	this->_timeExpired = true;
}
