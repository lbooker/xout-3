//
//  GameNode.cpp
//  xout-3
//
//  Created by Lewis Booker on 5/8/14.
//
//

#include "ButtonNode.h"
#include <string>
using namespace std;
using namespace cocos2d;



ButtonNode* ButtonNode::createButton(const std::string& filename, int id)
{
    auto node = (ButtonNode*)Sprite::create(filename);
    node->setId(id);
    return node;
}

int ButtonNode::getId()
{
    return _id;
}

void ButtonNode::setId(int id)
{
    _id = id;
}