
#ifndef __xout__ShapeOptions__
#define __xout__ShapeOptions__

#include "cocos2d.h"
#include <functional>

using namespace cocos2d;

class ShapeOptions : public cocos2d::Layer
{

private:
	Size _screenSize;

	void registerClickHandlers();

public:

	// there's no 'id' in cpp, so we recommend to return the class instance pointer
	static cocos2d::Scene* scene();
	virtual bool init();
	// preprocessor macro for "static create()" constructor ( node() deprecated )
	CREATE_FUNC(ShapeOptions);

};

#endif /* defined(__xout__ShapeOptions__) */
