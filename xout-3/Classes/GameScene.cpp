#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "Util.h"
#include "xNode.h"
#include <functional>

using namespace cocos2d;
using namespace CocosDenshion;

const int SCORE_FONT_SIZE = 48;
const float TIMEOUT = 3.0;
const float DIFFICULTY = 0.7;

int calculateLevel(int score)
{
	if (score < 1) return 1;
	if (score < 3) return 2;
	if (score < 10) return 3;
	return 4;
}

float calculateInterval(int level)
{
	return 0.4;
}

float calculateTimeout(int level)
{
	return 10;
}

int calculateLevelClearance(int level)
{
    return 2 * level - 1;
}

int calculateShape1and2Count(int level)
{
	if (level < 3) return 0;

	return 2 * (level - 2) - 1;
}

int calculateDoubleTaps(int level)
{
	if (level < 5) return 0;

	return (level - 5) * 2 + 1;

}

int calculateTripleTaps(int level)
{
	if (level < 7) return 0;

	return (level - 7) * 2 + 1;
}

bool isRandomHit(int sampleSize)
{
	int r = rand_range(1, sampleSize);
	return r == sampleSize;
}

Scene* GameScene::scene(int score, int level)
{

    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameScene::create();

	layer->_score = score;
	layer->_level = level;
	layer->displayScore();

    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
	//return true;

    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    _screenSize = Director::getInstance()->getWinSize();
    
	_scoreLabel = Label::createWithSystemFont("Score:", "Arial", 32);
    _scoreLabel->setPosition(Point(100, _screenSize.height * .95));
    _scoreLabel->setColor( Color3B(255,255,255) );
    this->addChild(_scoreLabel);
    
	_scoreBoardLabel = Label::createWithSystemFont("0", "Arial", 32);
	auto scoreLabelLocation = _scoreLabel->getPosition();
	_scoreBoardLabel->setPosition(Point(scoreLabelLocation.x + 100, scoreLabelLocation.y));
	_scoreBoardLabel->setColor(Color3B(255, 255, 255));
	_scoreBoardLabel->setVisible(false);
	this->addChild(_scoreBoardLabel);

	_levelLabel = Label::createWithSystemFont("Level:", "Arial", 32);
	_levelLabel->setPosition(Point(100, _scoreLabel->getPosition().y - 40));
	_levelLabel->setColor(Color3B(255, 255, 255));
	this->addChild(_levelLabel);

	_levelBoardLabel = Label::createWithSystemFont("0", "Arial", 32);
	Point levelLabelLocation = _levelLabel->getPosition();
	_levelBoardLabel->setPosition(Point(levelLabelLocation.x + 100, levelLabelLocation.y));
	_levelBoardLabel->setColor(Color3B(255, 255, 255));
	this->addChild(_levelBoardLabel);

	_startButton = Sprite::create("start-button.jpg");
	_startButton->setScale(1);
	_startButton->setPosition(Point(_screenSize.width * .5, _screenSize.height * .5));
	this->addChild(_startButton, 99);

	_gameOverLabel = Label::createWithSystemFont("Game Over", "Arial", 32);
	_gameOverLabel->setPosition(Point(_screenSize.width * .50, _screenSize.height * .25));
	_gameOverLabel->setColor(Color3B(255, 255, 255));
	_gameOverLabel->setVisible(false);
	this->addChild(_gameOverLabel);
    
	_xs = Vector<xNode *>();
	_ts = Vector<xNode *>();
	_comets = Vector<Sprite *>();
    registerClickHandlers();
    
    return true;
}

void GameScene::registerClickHandlers() {

	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = [&](Touch* touch, Event* event){
		if (!_comets.empty()) return false;
		_lock = true;
		auto target = static_cast<GameScene*>(event->getCurrentTarget());
		//Get the position of the current point relative to the button
		Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		Point touchPoint = touch->getLocation();
		bool xHit = false;
		_lock = true;

		int lowerBound = 0;
		int upperBound = _xs.size();
		for (int i = lowerBound; i < upperBound; i++)
		{
			auto x = _xs.at(i);
			Rect r = x->getBoundingBox();
			float radius = 0.7 * r.size.width;
			float distance = touchPoint.getDistance(x->getPosition());
			if (distance <= radius) {
				x->tapCount++;
				if (x->tapCount < x->maxTaps)
				{
					xHit = true;
					if (x->maxTaps - x->tapCount == 1)
					{
						x->setColor(WHITE);						
					}
					if (x->maxTaps - x->tapCount == 2)
					{
						x->setColor(GREEN);
					}
					break;
				}
				xHit = true;
				x->isHit = true;
				removeChild(x);
				_score++;
				_levelScore++;
				displayScore();
				break;
			}
		}
		if (!xHit)
		{
			this->endGame();
			return false;
		}

		_lock = false;
        
		_xs.eraseObjects([](xNode* n){return n->isHit; }, true);

		if (_levelScore == calculateLevelClearance(_level))
		{
			changeLevels();
		}

		return false;
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	auto listener1 = EventListenerTouchOneByOne::create();
	listener1->setSwallowTouches(true);
	listener1->onTouchBegan = [&](Touch* touch, Event* event){
		auto target = static_cast<Sprite*>(event->getCurrentTarget());
		if (target->isVisible() == false) {
			return false;
		}
		//Get the position of the current point relative to the button
		Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		Size s = target->getContentSize();
		Rect rect = Rect(0, 0, s.width, s.height);
		//Check the click area
		if (rect.containsPoint(locationInNode))
		{
			this->startGame();
			return true;
		}
		return false;
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, _startButton);



}

void GameScene::changeLevels()
{
    _level++;
	_levelScore = 0;
	_shape1and2Count = 0;
	_doubleTapCount = 0;
	moveComet();
}

void GameScene::moveComet()
{
	this->unschedule(schedule_selector(GameScene::delayPlaceX));
	this->unschedule(schedule_selector(GameScene::delayPlaceT));

	if (_startButton->isVisible()) return;

	_lock = true;
	clearScreen();

	Sprite* comet = Sprite::create("comet.jpg");
	comet->setScale(0.3);
	_comets.pushBack(comet);
	
	//int y = rand_range(200, _screenSize.height - 50);
	int y = 250;

	comet->setPosition(-1, y);
	this->addChild(comet);
	float duration = 2.0;

	FiniteTimeAction* actionMove = MoveTo::create(duration, Point(_screenSize.width + 1, y));
	FiniteTimeAction* moveDone = CallFuncN::create(this, callfuncN_selector(GameScene::moveCometFinished));
	actionMove->setTag(1);
	moveDone->setTag(2);

	auto listener1 = EventListenerTouchOneByOne::create();
	listener1->setSwallowTouches(true);
	listener1->onTouchBegan = [&](Touch* touch, Event* event){
		auto target = static_cast<Sprite*>(event->getCurrentTarget());
		if (target->isVisible() == false) {
			return false;
		}
		//Get the position of the current point relative to the button
		Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		Size s = target->getContentSize();
		Rect rect = Rect(0, 0, s.width, s.height);
		//Check the click area
		if (rect.containsPoint(locationInNode))
		{
			target->setColor(Color3B(50, 100, 500));
            _score += 5;
            displayScore();
			//moveComet();
			//target->stopAction(actionMove);
			return true;
		}
		return false;
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, comet);

	
	comet->runAction(Sequence::create(actionMove, moveDone, NULL));

}

void GameScene::moveCometFinished(Node* sender)
{

	displayScore();
	Sprite* comet = (Sprite*)sender;
	removeChild(comet);
	_comets.eraseObject(comet);
	_lock = false;

	float interval = calculateInterval(_level);
	this->schedule(schedule_selector(GameScene::delayPlaceX), interval);

}

void GameScene::update(float dt)
{
	if (_lock) return;

	bool doExitGame = false;
	for (auto x: _xs)
	{
		if (x->getTimeExpired())
		{
			doExitGame = true;
			break;
		}
	}

	for (auto t : _ts)
	{
		if (t->getTimeExpired())
		{
			this->removeChild(t);
		}
	}

	_ts.eraseObjects([](xNode* n){return n->getTimeExpired(); }, true);

	if (doExitGame) this->endGame();
	
}

void GameScene::startGame() {
	_lock = false;
	_levelScore = 0;
	_gameOverLabel->setVisible(false);
	_startButton->setVisible(false);
	_score = 0;
	_level = 1;
	displayScore();
	scheduleUpdate();
	xNode* x = xNode::createX("close-x.png", calculateTimeout(_level));
	_xs.pushBack(x);
	placeX(x);
	//schedule(schedule_selector(GameScene::delayPlaceX), 2);
	//schedule(schedule_selector(GameScene::update), 0.10);
}

void GameScene::displayScore() {
	string score = to_string(_score);
	_scoreBoardLabel->setVisible(true);
	_scoreBoardLabel->setString(score);

	_levelBoardLabel->setString(to_string(_level));

}


void GameScene::endGame() {
	_lock = true;
    _gameOverLabel->setVisible(true);
	this->unscheduleAllSelectors();
	this->clearScreen();
	_level = 1;
	_startButton->setVisible(true);
}

void GameScene::clearScreen() 
{
	bool lock = _lock;
	_lock = true;
	for(auto x: _xs)
	{
		this->removeChild(x);
	}
    for(auto t: _ts)
    {
        this->removeChild(t);
    }
	_xs.clear();
    _ts.clear();
	_lock = lock;
}

void GameScene::timeExpired(float dt)
{
    endGame();
}

void GameScene::placeX(Sprite* x) {

	if (_lock) return;

	//int maxCount = calculateLevelClearance(_level);
	//if (maxCount > 1)
	//{
	//	if ((_xs.size() + _levelScore) >= maxCount)
	//	{
	//		return;
	//	}
	//}

	int minX = 50;
	int maxX = _screenSize.width * 0.8;
	int minY = 30;
	int maxY = _levelLabel->getPositionY() - 80;

	Size size = x->getContentSize();
	float scale = 1.5;
	x->setScale(scale);
	int width = rand_range(minX, maxX);
	int height = rand_range(minY, maxY);
	x->setPosition(Point(width, height));
	x->setVisible(true);


	// add the sprite as a child to this layer
	this->addChild(x, 0);
	x->retain();
}

void GameScene::delayPlaceX(float dx)
{
	if (_level > 3 && isRandomHit(4)) return;

	if (_startButton->isVisible())
	{
		this->unscheduleAllSelectors();
		this->clearScreen();
		return;
	}
	if (_lock) return;
	int maxCount = calculateLevelClearance(_level);
	if ((_xs.size() + _levelScore) >= maxCount) return;
	xNode* x = xNode::createX("close-x.png", calculateTimeout(_level));
	_xs.pushBack(x);
	this->placeX(x);
	if (isRandomHit(3))
	{
		this->schedule(schedule_selector(GameScene::delayPlaceT), calculateInterval(_level));
	}

	int maxDoubleTaps = calculateDoubleTaps(_level);
	if (_doubleTapCount < maxDoubleTaps && isRandomHit(3))
	{
		this->scheduleOnce(schedule_selector(GameScene::delayPlaceDoubleTaps), calculateInterval(_level));
	}

	int maxTripleTaps = calculateTripleTaps(_level);
	if (_tripleTapCount < maxTripleTaps && isRandomHit(3))
	{
		this->scheduleOnce(schedule_selector(GameScene::delayPlaceTripleTaps), calculateInterval(_level));
	}

}

void GameScene::placeTapX(int numberOfTaps)
{
	xNode* x = xNode::createX("close-x.png", calculateTimeout(_level), numberOfTaps);

}

void GameScene::delayPlaceDoubleTaps(float dx)
{
	int maxCount = calculateLevelClearance(_level);
	if ((_xs.size() + _levelScore) >= maxCount) return;
	xNode* x = xNode::createX("close-x.png", calculateTimeout(_level), 2);
	_xs.pushBack(x);
	this->placeX(x);
	_doubleTapCount++;

}

void GameScene::delayPlaceTripleTaps(float dx)
{
	int maxCount = calculateLevelClearance(_level);
	if ((_xs.size() + _levelScore) >= maxCount) return;
	xNode* x = xNode::createX("close-x.png", calculateTimeout(_level), 3);
	_xs.pushBack(x);
	this->placeX(x);
	_tripleTapCount++;
}

void GameScene::delayPlaceT(float dx)
{

	int otherShapeCount = calculateShape1and2Count(_level);
	if (_shape1and2Count >= otherShapeCount) return;

	if (_startButton->isVisible())
	{
		this->unscheduleAllSelectors();
		this->clearScreen();
		return;
	}
	if (_lock) return;

	Color3B color = WHITE;
	if (_doubleTapCount > 0 && _tripleTapCount > 0)
	{
		if (isRandomHit(2))
			color = GREEN;
		else
			color = YELLOW;
	}
	else if (_doubleTapCount > 0) color = GREEN;
	
	xNode* s;

	if (isRandomHit(2))
		s = xNode::createOther("triangle.png", calculateTimeout(_level), color);
	else if (isRandomHit(2))
		s = xNode::createOther("equals.png", calculateTimeout(_level), color);
	else
		s = xNode::createOther("square.png", calculateTimeout(_level), color);

	s->setColor(color);
	_ts.pushBack(s);
	_shape1and2Count++;
	this->placeX(s);
}

