#include "ShapeOptions.h"
#include "SimpleAudioEngine.h"
#include "Util.h"
#include "GameScene.h"
#include <functional>

using namespace cocos2d;
using namespace CocosDenshion;


Scene* ShapeOptions::scene()
{

	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = ShapeOptions::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool ShapeOptions::init()
{
	//return true;

	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	_screenSize = Director::getInstance()->getWinSize();

	int row = 0;
	int radius = 20;
	int columns = _screenSize.width / (radius * 2);
	int circles = 30;
	int circle = 0;

	while (circle < circles) {
		for (int i = 0; i < columns; i++) {
			int x = i * radius * 2 + radius;
			int y = _screenSize.height - (row * radius * 2 + radius);
			Point* p = new Point(x, y);
			DrawNode* dn = DrawNode::create();
			dn->drawDot(*p, radius, Color4F::GRAY);
			this->addChild(dn, 100);
			Label* number = Label::createWithSystemFont(to_string(circle + 1), "Arial", 22);
			number->setPosition(x, y);
			number->setColor(Color3B(255, 255, 255));
			this->addChild(number, 101);
			circle++;
			if (circle == circles) break;
		}
		row++;
	}


	//registerClickHandlers();

}

void ShapeOptions::registerClickHandlers() {

	auto listener1 = EventListenerTouchOneByOne::create();
	listener1->setSwallowTouches(true);
	listener1->onTouchBegan = [&](Touch* touch, Event* event){
		auto target = static_cast<Sprite*>(event->getCurrentTarget());
		if (target->isVisible() == false) {
			return false;
		}
		//Get the position of the current point relative to the button
		Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		Size s = target->getContentSize();
		Rect rect = Rect(0, 0, s.width, s.height);
		//Check the click area
		if (rect.containsPoint(locationInNode))
		{
			Director::getInstance()->replaceScene(GameScene::scene(0, 1));
			return true;
		}
		return false;
	};
	//_eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, _classicButton);



}
