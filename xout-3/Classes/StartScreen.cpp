#include "StartScreen.h"
#include "SimpleAudioEngine.h"
#include "Util.h"
#include "GameScene.h"
#include "ShapeOptions.h"
#include <functional>

using namespace cocos2d;
using namespace CocosDenshion;


Scene* StartScreen::scene()
{

	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = StartScreen::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool StartScreen::init()
{
	//return true;

	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	_screenSize = Director::getInstance()->getWinSize();


	_classicButton = Sprite::create("classicButton.png");
	_classicButton->setScale(1);
	_classicButton->setPosition(Point(_screenSize.width * .5, _screenSize.height * .8));
	this->addChild(_classicButton, 99);

	_shapesButton = Sprite::create("shapesButton.png");
	_shapesButton->setScale(1);
	_shapesButton->setPosition(Point(_screenSize.width * .5, _classicButton->getPositionY() - 300));
	//this->addChild(_shapesButton, 99);

	registerClickHandlers();

	return true;
}

void StartScreen::registerClickHandlers() {

	auto listener1 = EventListenerTouchOneByOne::create();
	listener1->setSwallowTouches(true);
	listener1->onTouchBegan = [&](Touch* touch, Event* event){
		auto target = static_cast<Sprite*>(event->getCurrentTarget());
		if (target->isVisible() == false) {
			return false;
		}
		//Get the position of the current point relative to the button
		Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		Size s = target->getContentSize();
		Rect rect = Rect(0, 0, s.width, s.height);
		//Check the click area
		if (rect.containsPoint(locationInNode))
		{
			Director::getInstance()->replaceScene(GameScene::scene(0, 1));
			return true;
		}
		return false;
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, _classicButton);

	auto listener2 = EventListenerTouchOneByOne::create();
	listener2->setSwallowTouches(true);
	listener2->onTouchBegan = [&](Touch* touch, Event* event){
		auto target = static_cast<Sprite*>(event->getCurrentTarget());
		if (target->isVisible() == false) {
			return false;
		}
		//Get the position of the current point relative to the button
		Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		Size s = target->getContentSize();
		Rect rect = Rect(0, 0, s.width, s.height);
		//Check the click area
		if (rect.containsPoint(locationInNode))
		{
			Director::getInstance()->replaceScene(ShapeOptions::scene());
			return true;
		}
		return false;
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener2, _shapesButton);



}
