
#ifndef __xout__StartScreen__
#define __xout__StartScreen__

#include "cocos2d.h"
#include <functional>

using namespace cocos2d;

class StartScreen : public cocos2d::Layer
{

private:
	Sprite * _classicButton;
	Sprite * _shapesButton;
	Size _screenSize;

	void registerClickHandlers();

public:

	// there's no 'id' in cpp, so we recommend to return the class instance pointer
	static cocos2d::Scene* scene();
	virtual bool init();
	// preprocessor macro for "static create()" constructor ( node() deprecated )
	CREATE_FUNC(StartScreen);

};

#endif /* defined(__xout__StartScreen__) */
