//
//  xNode.h
//  xout-3
//
//  Created by Lewis Booker on 5/14/14.
//
//

#ifndef __xout_3__xNode__
#define __xout_3__xNode__

#include "cocos2d.h"

using namespace cocos2d;
using namespace std;

#define GREEN Color3B(10, 245, 50)
#define YELLOW Color3B(247, 243, 7)
#define WHITE Color3B(255, 255, 255)

class xNode : public cocos2d::Sprite
{

    void timeExpired(float dt);
    void warning(float dt);
	bool _timeExpired;
    public:
		static xNode * createOther(const string& filename, float timeout, Color3B color);
		static xNode * createX(const string& filename, float timeout);
		static xNode * createX(const string& filename, float timeout, int maxTaps);
		bool isHit;
		bool getTimeExpired();
		int tapCount;
		int maxTaps;
        void initX(float timeout);
};

#endif /* defined(__xout_3__xNode__) */
