//
//  ButtonNode.h
//  xout-3
//
//  Created by Lewis Booker on 5/8/14.
//
//

#ifndef __xout_3__ButtonNode__
#define __xout_3__ButtonNode__

#include "cocos2d.h"
#include <functional>

using namespace std;

class ButtonNode : public cocos2d::Sprite
{
    int _id;
    
    public:
        static ButtonNode * createButton(const string& filename, int id);
        int getId();
        void setId(int id);
};

#endif /* defined(__xout_3__ButtonNode__) */
