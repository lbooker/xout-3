//
//  Util.h
//  xout
//
//  Created by Lewis Booker on 4/20/14.
//
//

#ifndef __xout__Util__
#define __xout__Util__

#include <iostream>

int rand_range(int min_n, int max_n);

#endif /* defined(__xout__Util__) */
