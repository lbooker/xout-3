//
//  GameScene.h
//  xout
//
//  Created by Lewis Booker on 5/3/14.
//
//

#ifndef __xout__GameScene__
#define __xout__GameScene__

#include "cocos2d.h"
#include "xNode.h"
#include <functional>

using namespace cocos2d;

class GameScene : public cocos2d::Layer
{
    
private:
	bool _lock = false;
	Vector<xNode *> _xs;
	Vector<xNode *> _ts;
	Vector<Sprite *> _comets;
	int _level = 1;
	int _score = 0;
	int _levelScore = 0;
	int _shape1and2Count = 0;
	int _doubleTapCount = 0;
	int _tripleTapCount = 0;
	Size _screenSize;
	Label * _scoreLabel;
	Label * _scoreBoardLabel;
	Label * _gameOverLabel;
	Label * _levelLabel;
	Label * _levelBoardLabel;
	Label * _lineLabel;
	Label * _continueLabel;
	Sprite * _continueButton;
	Sprite * _startButton;
	Sprite * _closeButton;

	void registerClickHandlers();
	void moveComet();
	void moveCometFinished(Node*);
	void changeLevels();

public:

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
	static cocos2d::Scene* scene(int score, int level);
	virtual bool init();
	void startGame();
    void endGame();
	void displayScore();
	void clearScreen();
	void placeX(Sprite* node);
	virtual void update(float dt);
	void timeExpired(float dt);
	void placeTapX(int numberOfTaps);
	void delayPlaceX(float dx);
	void delayPlaceT(float dx);
	void delayPlaceDoubleTaps(float dx);
	void delayPlaceTripleTaps(float dx);
	// preprocessor macro for "static create()" constructor ( node() deprecated )
	CREATE_FUNC(GameScene);

};

#endif /* defined(__xout__GameScene__) */
